from django.conf.urls import patterns, url
from main_site.views import mainClass

urlpatterns = patterns('main_site.views',
    url(r'', mainClass().mainView),
)