#!/usr/bin/python
# -*- coding: UTF-8 -*-

import thread

from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from fabric.api import *
from fabric.context_managers import *
from fabric.network import *
from fabric.state import *

from server.form import ServerUnitForm
from usuarios.form import UserLoginForm


GLOBAL_HOST = 'dmarquez@ssh2.inf.utfsm.cl'

JS_PORT = "9999"
JS_USER = "dmarquez"
JS_SERVER = "ssh2.inf.utfsm.cl"
JS_JUMP_USER = "django-spa-style"
JS_JUMP_SERVER = "10.10.11.159"

JSR_JUMP_USER = "root"
JSR_JUMP_SERVER = "10.10.11.159"

GSPL_JUMP_USER = "django-spa-style"
GSPL_JUMP_SERVER = "10.10.11.159"
GSPL_FOLDER = "/home/django-spa-style/tsclosnumerosspastyle"

DSS_JUMP_USER = "django-spa-style"
DSS_JUMP_SERVER = "10.10.11.159"
DSS_PROJECT_FOLDER = "/home/django-spa-style/tsclosnumerosspastyle/django_spa_style"

class mainClass():
    def mainView(self, request):
        form = UserLoginForm()
        user_login_form =  {'form' : form}
        user_login_form.update(csrf(request))
        render_form = render_to_response('user_login_form_mini.html', user_login_form, context_instance = RequestContext(request))
        vars = { 'form' : render_form.content }
        vars.update(csrf(request))
        
        #thread.start_new_thread( js, () )
        return render_to_response('main.html', vars)

def js(p=JS_PORT,s=JS_SERVER,js=JS_JUMP_SERVER,u=JS_USER,ju=JS_JUMP_USER):
    try:  
        local("ssh -D "+p+" -t "+u+"@"+s+" pwd")
    except Exception as e: print e