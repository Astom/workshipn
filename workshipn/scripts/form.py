#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.forms.models import ModelForm
from django.forms.widgets import TextInput

from scripts.models import Script
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.template.context import RequestContext

class ScriptForm(ModelForm):
    class Meta:
        model = Script
        include = ('condition', 'script')
        exclude = ('name','description')
        widgets = { 
            'condition' : TextInput(attrs={'placeholder': 'Condition','class': 'form-control'}), 
            'script' : TextInput(attrs={'placeholder': 'Script','class': 'form-control'}), 
            }
        
class EditScriptForm(ModelForm):
    id = forms.IntegerField(widget=TextInput(attrs={'class': 'form-control hide'}))
    class Meta:
        model = Script
        include = ('condition', 'script','id')
        exclude = ('name','description')
        widgets = { 
            'condition' : TextInput(attrs={'placeholder': 'Condition','class': 'form-control'}), 
            'script' : TextInput(attrs={'placeholder': 'Script','class': 'form-control'}), 
            'id' : TextInput(attrs={'placeholder': 'Condition','class': 'form-control hide'}),
        }
        
    def renderEditScriptList(self, request, server_group):
        group_scripts = server_group.scripts.all()
        script_forms = []
        for script in group_scripts:
            form = EditScriptForm(instance=script)
            script_forms.append(form)
        dict = {'forms' : script_forms[::-1], 'server_group':server_group}
        dict.update(csrf(request))
        render_script_edit_forms = render_to_response('script_edit_list_form.html', dict, context_instance = RequestContext(request))
        return render_script_edit_forms.content