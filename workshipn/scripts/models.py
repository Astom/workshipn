#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.db import models

class Script(models.Model):
    name = models.CharField(max_length = 100, blank=True, null=True)
    condition = models.CharField(max_length = 10000, blank=True, null=True)
    script = models.CharField(max_length = 10000, blank=False, null=False)
    description = models.CharField(max_length = 1000, blank=True, null=True)