#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.conf.urls import patterns, url
from scripts.views import mainClass

urlpatterns = patterns('scripts.views',
    url(r'newscript/{0,1}', mainClass().newScript),
    url(r'playscript/{0,1}', mainClass().playScript),
    url(r'editscript/{0,1}', mainClass().editStript),
    url(r'deletescript/{0,1}', mainClass().deleteStript),
)