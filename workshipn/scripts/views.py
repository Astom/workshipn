#!/usr/bin/python
# -*- coding: UTF-8 -*-

from StringIO import StringIO

from django import forms
from django.contrib.auth.models import AnonymousUser
from django.core.context_processors import csrf
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import simplejson

import fabric.api as fb
import fabric.tasks as fbt
from scripts.form import ScriptForm, EditScriptForm
from server_group.models import GroupServer
from ansi2html import Ansi2HTMLConverter

def executeCommand(command):
    try:
        myout = StringIO()
        result = fb.run(command,
                   combine_stderr=myout, 
                   quiet=True, 
                   warn_only=True, 
                   stdout=myout, 
                   stderr=myout)
        return result
    except Exception as e:
        print e
        print "error al ejecutar el run" 
        
class mainClass():
    def editStript(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                form = EditScriptForm(request.POST)
                group = forms.IntegerField().clean(request.GET['servergroup'])
                if form.is_valid():
                    script = form.save(commit=False)
                    script.id = form.cleaned_data['id']
                    script.save()
                    server_group = GroupServer.objects.get(id=group)
                    message = {"success":True, 'forms':EditScriptForm().renderEditScriptList(request, server_group)}
                    json_r = simplejson.dumps(message)
                    return HttpResponse(json_r, 'application/json')
                else:
                    message = {"success":True, 'forms':EditScriptForm().renderEditScriptList(request, server_group)}
                    json_r = simplejson.dumps(message)
                    return HttpResponse(json_r, 'application/json')
            return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')
        
    def deleteStript(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                form = EditScriptForm(request.POST)
                group = forms.IntegerField().clean(request.GET['servergroup'])           
                
                if form.is_valid():
                    script = form.save(commit=False)
                    script.id = form.cleaned_data['id']
                    script.delete()
                    server_group = GroupServer.objects.get(id=group)
                    message = {"success":True, 'forms':EditScriptForm().renderEditScriptList(request, server_group)}
                    json_r = simplejson.dumps(message)
                    return HttpResponse(json_r, 'application/json')
            return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')
        
    def playScript(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                form = EditScriptForm(request.POST)
                group = forms.IntegerField().clean(request.GET['servergroup'])
                if form.is_valid():
                    try:
                        script = form.save(commit=False)
                        script.id = form.cleaned_data['id']
                        server_group = GroupServer.objects.get(id=group)
                        hosts = server_group.serverList()
                        results = fbt.execute(executeCommand, command=script.script, hosts=hosts)
                        html = ''
                        conv = Ansi2HTMLConverter()
                        for result in results:
                            text = "".join(results[result])
                            text = text.decode("UTF-8")
                            html = "<pre>" + result + "</pre> " + conv.convert(text) +'<br>' + html
                        message = {"success":True,
                                   'forms':EditScriptForm().renderEditScriptList(request, server_group),
                                   'html':html}
                        json_r = simplejson.dumps(message)
                        return HttpResponse(json_r, 'application/json')
                    except:
                        server_group = GroupServer.objects.get(id=group)
                        message = {"success":False, 'forms':EditScriptForm().renderEditScriptList(request, server_group)}
                        json_r = simplejson.dumps(message)
                        return HttpResponse(json_r, 'application/json')
            return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')           
    
    def newScript(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group = forms.IntegerField().clean(request.GET['servergroup'])
                server_group = GroupServer.objects.get(id=group)
                form = ScriptForm(request.POST)
                if form.is_valid() :
                    script = form.save(commit=False)
                    script.save()
                    server_group.scripts.add(script)
                    
                    form = ScriptForm()
                    script_form = {'form':form}
                    script_form.update(csrf(request))
                    render_script_form = render_to_response('script_form.html', script_form, context_instance = RequestContext(request))
                    
                    message = {"success":True, 'form':render_script_form.content, 'forms':EditScriptForm().renderEditScriptList(request, server_group)}
                    json_r = simplejson.dumps(message)
                    
                    return HttpResponse(json_r, 'application/json')
                else:
                    script_form = {'form':form}
                    script_form.update(csrf(request))
                    render_script_form = render_to_response('script_form.html', script_form, context_instance = RequestContext(request))
                    message = {"success":False, 'form':render_script_form.content}
                    json_r = simplejson.dumps(message)
                    return HttpResponse(json_r, 'application/json')
            return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')
