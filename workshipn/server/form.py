#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.forms import ModelForm
from django.forms.widgets import TextInput, Select

from server.models import ServerUnit
from server_group.models import GroupServer
from usuarios.models import UserGroup


class ServerUnitForm(ModelForm):
    server_group = forms.ModelChoiceField
    
    def __init__(self, *args, **kwargs):
        super(ServerUnitForm, self).__init__(*args, **kwargs)
        try:
            user_groups = UserGroup.objects.filter(owner=self.initial['user'])
        except:
            user_groups = UserGroup.objects.filter(owner=args[0]['user'])
        server_groups = GroupServer.objects.filter(user_group__in = user_groups)
        self.fields['server_group'] = forms.ModelChoiceField(queryset=server_groups, 
                                                                 widget=Select(attrs={'placeholder': 'Group Name','class': 'form-control'}))
        
    class Meta:
        model = ServerUnit
        include = ('server_name', 'ip','user_name', 'select')
        exclude = ('port')
        widgets = {
            'server_name' : TextInput(attrs={'placeholder': 'Server Name','class': 'form-control'}),
            'ip' : TextInput(attrs={'placeholder': 'Ip','class': 'form-control'}),
            'user_name' : TextInput(attrs={'placeholder': 'User Name','class': 'form-control'}), 
        }
        
class ServerUnitInGroupForm(ModelForm):
    server_group = forms.ModelChoiceField
    
    def __init__(self, *args, **kwargs):
        super(ServerUnitInGroupForm, self).__init__(*args, **kwargs)
        try:
            server_groups = GroupServer.objects.filter(id = self.initial['group_id'])
            self.fields['server_group'] = forms.ModelChoiceField(queryset=server_groups, 
                                                                 widget=Select(attrs={'class': 'form-control hide'}),
                                                                 label='',
                                                                 initial=self.initial['group_id'])
        except:
            pass
        
    class Meta:
        model = ServerUnit
        include = ('server_name', 'ip','user_name', 'select')
        exclude = ('port')
        widgets = {
            'server_name' : TextInput(attrs={'placeholder': 'Server Name','class': 'form-control'}),
            'ip' : TextInput(attrs={'placeholder': 'Ip','class': 'form-control'}),
            'user_name' : TextInput(attrs={'placeholder': 'User Name','class': 'form-control'}), 
        }

class ServerUnitEditForm(ModelForm):
    server_group = forms.ModelChoiceField
    id = forms.IntegerField(widget=TextInput(attrs={'placeholder': 'Condition','class': 'form-control hide'}), label='')
    
    def __init__(self, *args, **kwargs):
        super(ServerUnitEditForm, self).__init__(*args, **kwargs)
        try:
            server_groups = GroupServer.objects.filter(id = self.initial['group_id'])
            self.fields['server_group'] = forms.ModelChoiceField(queryset=server_groups, 
                                                                 widget=Select(attrs={'class': 'form-control hide'}),
                                                                 initial=self.initial['group_id'],
                                                                 label='')
        except:
            pass
        
    class Meta:
        model = ServerUnit
        include = ('server_name', 'ip','user_name', 'select','id','server_group')
        exclude = ('port')
        widgets = {
            'server_name' : TextInput(attrs={'placeholder': 'Server Name','class': 'form-control'}),
            'ip' : TextInput(attrs={'placeholder': 'Ip','class': 'form-control'}),
            'user_name' : TextInput(attrs={'placeholder': 'User Name','class': 'form-control'}),
            'id' : TextInput(attrs={'class': 'form-control hide'}),
        }