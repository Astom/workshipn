#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.db import models

class ServerUnit(models.Model):
    server_name = models.CharField(max_length = 150)
    ip = models.CharField(max_length = 150)
    port = models.IntegerField(max_length = 10, blank=True, null=True)
    user_name = models.CharField(max_length = 50)
    
    def __unicode__(self):
        return self.server_name