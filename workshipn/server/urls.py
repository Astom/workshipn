from django.conf.urls import patterns, url
from server.views import mainClass

urlpatterns = patterns('server.views',
    url(r'newserver/{0,1}', mainClass().newServer),
    url(r'editserversave/{0,1}', mainClass().editServerSave),
    url(r'editserver/{0,1}', mainClass().editServerWindow),
    url(r'', mainClass().serverGridList),
)