#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.contrib.auth.models import AnonymousUser
from django.core.context_processors import csrf
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import simplejson

from server.form import ServerUnitForm, ServerUnitEditForm
from server.models import ServerUnit
from server_group.form import ServerFromGroupForm
from server_group.models import GroupServer
from server_group.views import serverGroupClass


class mainClass():
    def editServerSave(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                form = ServerUnitEditForm(request.POST)
                group_id = forms.IntegerField().clean(request.POST['server_group']) 
                if form.is_valid():
                    server = ServerUnit.objects.get(id=form.cleaned_data['id'])
                    server.server_name = form.cleaned_data['server_name']
                    server.ip = form.cleaned_data['ip']
                    server.save()
                    servers_renders = serverGroupClass().renderGroupServers(request, GroupServer.objects.get(id=group_id))
                    html = ''
                    for render in servers_renders:
                        html = html + render.content
                    message = {"success":True, "servers":html}
                else:
                    message = {"success":False}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json')
            else:
                return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
    
    def editServerWindow(self, request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                form = ServerFromGroupForm(request.POST)
                if form.is_valid():
                    server = ServerUnit.objects.get(id=form.cleaned_data['server_id'])
                    form = ServerUnitEditForm(instance=server, initial={'group_id':form.cleaned_data['group_id']})
                    dictionary = {'form':form}
                    dictionary.update(csrf(request))
                    render_form = render_to_response('server_form.html', dictionary, context_instance = RequestContext(request))
                    message = {"success":True, 'form':render_form.content}
                else:
                    message = {"success":False}
                json_r = simplejson.dumps(message) 
                return HttpResponse(json_r, 'application/json')
            else:
                return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
    
    def serverGridList(self, request):
        if request.method == 'POST':
            form = ServerUnitForm(request.POST)
        else:
            form = ServerUnitForm()
        vars = {'form':form}
        vars.update(csrf(request))
        return render_to_response('server_grid_list.html', vars)
    
    def newServer(self, request):
        if request.is_ajax():
            post_temp = request.POST.copy()
            post_temp['user'] = request.user
            form = ServerUnitForm(post_temp)
            if form.is_valid():
                server = form.save(commit=False)
                server_group = form.cleaned_data['server_group']
                server.save()
                server_group.servers.add(server)
                message = {"success":True}
            else:
                new_server_form =  {'form' : form}
                new_server_form.update(csrf(request))
                render_form = render_to_response('server_form.html', new_server_form, context_instance = RequestContext(request))
                message = {"success":False,"form": render_form.content}
            json_r = simplejson.dumps(message)
        else:
            json_r  = 'fail'
        return HttpResponse(json_r, 'application/json')
