#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.forms.forms import Form
from django.forms.models import ModelForm, ModelChoiceField
from django.forms.widgets import TextInput, Select

from server_group.models import GroupServer
from usuarios.models import UserGroup


class ServerGroupForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(ServerGroupForm, self).__init__(*args, **kwargs)
        try:
            self.fields['user_group'] = ModelChoiceField(queryset=UserGroup.objects.filter(owner=self.initial['user']),
                                                         widget=Select(attrs={'placeholder': 'Group Name','class': 'form-control'}))
        except:
            self.fields['user_group'] = ModelChoiceField(queryset=UserGroup.objects.filter(owner=args[0]['user']),
                                                         widget=Select(attrs={'placeholder': 'Group Name','class': 'form-control'}))
        
    class Meta:
        model = GroupServer
        include = ('name','user_group')
        exclude = ('servers', 'scripts')
        widgets = {
            'name' : TextInput(attrs={'placeholder': 'Group Name','class': 'form-control'}),
            'user_group' : Select(attrs={'placeholder': 'Group Name','class': 'form-control'}),
        }

class SearchServerGroupForm(Form):
    server_group = forms.ModelChoiceField

    def __init__(self, *args, **kwargs):
        super(SearchServerGroupForm, self).__init__(*args, **kwargs)
        try:
            user_groups = UserGroup.objects.filter(owner=self.initial['user'])
            server_groups = GroupServer.objects.filter(user_group__in = user_groups)
            self.fields['server_group'] = forms.ModelChoiceField(queryset=server_groups, 
                                                                 widget=Select(attrs={'class': 'form-control'}))
        except:
            pass

class EditServerGroupForm(ModelForm):
    id = forms.IntegerField(widget=TextInput(attrs={'class': 'form-control hide'}), label='')
            
    class Meta:
        model = GroupServer
        include = ('name','user_group', 'id')
        exclude = ('servers', 'scripts')
        widgets = {
            'name' : TextInput(attrs={'placeholder': 'Group Name','class': 'form-control'}),
            'user_group' : Select(attrs={'class': 'form-control'}),
            'id' : Select(attrs={'class': 'form-control hide'}),
        }

class DeleteServerGroupForm(ModelForm):
    id = forms.IntegerField(widget=TextInput(attrs={'class': 'form-control hide'}), label='')
    
    class Meta:
        model = GroupServer
        include = ('id')
        exclude = ('servers', 'scripts','name','user_group')
        widgets = {
            'id' : TextInput(attrs={'class': 'form-control hide'}),
        }
        
class ServerFromGroupForm(Form):
    server_id = forms.IntegerField()
    group_id = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(ServerFromGroupForm, self).__init__(*args, **kwargs)
        try:
            self.fields['group_id'] = forms.IntegerField(widget=TextInput(attrs={'class': 'form-control hide'}), initial=self.initial['group'].id)
            self.fields['server_id'] = forms.IntegerField(widget=TextInput(attrs={'class': 'form-control hide'}), initial=self.initial['server'].id)
        except:
            pass