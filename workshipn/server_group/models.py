#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.db import models

from scripts.models import Script
from server.models import ServerUnit
from usuarios.models import UserGroup

class GroupServer(models.Model):
    name = models.CharField(max_length = 100)
    user_group = models.ForeignKey(UserGroup, verbose_name = 'Owner')
    servers = models.ManyToManyField(ServerUnit)
    scripts = models.ManyToManyField(Script)
    
    def __unicode__(self):
        return self.name
    
    def serverList(self):
        servers = self.servers.all()
        l = []
        for server in servers:
            l.append(server.user_name + "@" + server.ip)
        return l
