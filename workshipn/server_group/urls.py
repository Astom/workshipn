#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.conf.urls import patterns, url
from server_group.views import mainClass

urlpatterns = patterns('server_group.views',
    url(r'newgroup/{0,1}', mainClass().newServerGroup),
    url(r'newserver/{0,1}', mainClass().newServer),
    url(r'servergroupservers/{0,1}', mainClass().serverGroupServers),
    url(r'myservergroups/{0,1}', mainClass().myServerGroups),
    url(r'openservergroup/{0,1}', mainClass().openServerGroup),
    url(r'exec/{0,1}', mainClass().executeScripts),
    url(r'addserversfromimport/{0,1}', mainClass().addServersFromImport),
    url(r'getgroupservers/{0,1}', mainClass().getGroupServers),
    url(r'editservergroup/{0,1}', mainClass().editServerGroup),
    url(r'editserver/{0,1}', mainClass().editServer),
    url(r'deleteservergroup/{0,1}', mainClass().deleteServerGroup),
    url(r'deleteserver/{0,1}', mainClass().deleteServer),
    url(r'', mainClass().myServerGroups),
)