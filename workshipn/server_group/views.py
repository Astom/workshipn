#!/usr/bin/python
# -*- coding: UTF-8 -*-

from StringIO import StringIO

from django import forms
from django.contrib.auth.models import AnonymousUser
from django.core.context_processors import csrf
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import simplejson

import fabric.api as fb
import fabric.tasks as fbt
from scripts.form import ScriptForm, EditScriptForm
from server.form import ServerUnitInGroupForm
from server.models import ServerUnit
from server_group.form import ServerGroupForm, ServerFromGroupForm, \
    DeleteServerGroupForm, EditServerGroupForm, SearchServerGroupForm
from server_group.models import GroupServer
from usuarios.models import UserGroup
from ansi2html import Ansi2HTMLConverter

fb.env.skip_bad_hosts = True

def executeCommand(command):
    try:
        myout = StringIO()
        result = fb.run(command,
                   combine_stderr=myout, 
                   quiet=True, 
                   warn_only=True, 
                   stdout=myout, 
                   stderr=myout)
        return result
    except Exception as e:
        print e
        print "error al ejecutar el run" 

class serverGroupClass():
    def renderGroupServers(self, request, server_group):
        group_servers = GroupServer.objects.get(id=server_group.id).servers.all()
        servers_render = []
        for server in group_servers:
            form_delete = ServerFromGroupForm(initial={'group':server_group,'server':server})
            dictionary = {'form' : form_delete, 'server':server}
            dictionary.update(csrf(request))
            servers_render.append(render_to_response('server_group_server_unit_info.html', dictionary, context_instance = RequestContext(request)))
        return servers_render
    
class mainClass():
    def addServersFromImport(self, request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group_id = forms.IntegerField().clean(request.POST['group_id'])
                server_group = GroupServer.objects.get(id = group_id)
                servers_id = []
                for server_id in request.POST.getlist('import_server'):
                    servers_id.append(forms.IntegerField().clean(server_id))
                servers = ServerUnit.objects.filter(id__in = servers_id)
                for server in servers:
                    try:
                        server_group.servers.add(server)
                        server_group.save()
                    except:
                        pass
                render_servers = serverGroupClass().renderGroupServers(request, server_group)
                html = ''
                for render in render_servers:
                    html = html + render.content
                
                message = {"success":True,
                           'servers':html}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json') 
        except Exception as e:
            message = {"success":False}
            json_r = simplejson.dumps(message)
            return HttpResponse(json_r, 'application/json') 
    
    def getGroupServers(self, request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group_id = forms.IntegerField().clean(request.POST['server_group'])
                server_group = GroupServer.objects.get(id=group_id)
                if server_group != None:
                    servers = server_group.servers.all()
                    dictionary = {'servers' : servers}
                    render_server_list = render_to_response('server_group_import_server_list.html',dictionary, context_instance = RequestContext(request))
                    message = {"success":True, 
                               "servers":render_server_list.content
                               }
                else:
                    message = {"success":False}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json') 
        except Exception as e:
            print e
            message = {"success":False}
            json_r = simplejson.dumps(message)
            return HttpResponse(json_r, 'application/json') 
    
    def editServerGroup(self,request):
        try:
            if request.method == 'POST' and request.user != AnonymousUser():
                form = EditServerGroupForm(request.POST)
                if form.is_valid():
                    server_group = GroupServer.objects.get(id=form.cleaned_data['id'])
                    server_group.name = form.cleaned_data['name']
                    server_group.user_group = form.cleaned_data['user_group']
                    server_group.save()
                    
                    form = EditServerGroupForm(instance=server_group)
                    dictionary = {'form' : form}
                    dictionary.update(csrf(request))
                    render_edit_server_form = render_to_response('server_group_form.html',dictionary, context_instance = RequestContext(request))
                    
                    group_name = "Edting " + server_group.name + "..."
                    
                    message = {"success":True,
                               'form':render_edit_server_form.content,
                               'form_name':group_name,
                               'name':server_group.name}
                else:
                    edit_server_form =  {'form' : form}
                    edit_server_form.update(csrf(request))
                    render_form = render_to_response('server_group_form.html', edit_server_form, context_instance = RequestContext(request))
                    message = {"success":False,"form": render_form.content}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json') 
        except Exception as e:
            print e
    
    def deleteServerGroup(self, request):
        try:
            if request.method == 'POST' and request.user != AnonymousUser():
                form = DeleteServerGroupForm(request.POST)
                if form.is_valid():
                    GroupServer.objects.get(id=form.cleaned_data['id']).delete()
                return HttpResponseRedirect('/user/panel')
        except Exception as e:
            print e
    
    def newServer(self, request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group_id = forms.IntegerField().clean(request.POST['server_group'])
                form = ServerUnitInGroupForm(request.POST, initial={'group_id':group_id})
                if form.is_valid():
                    server = form.save(commit=False)
                    server_group = form.cleaned_data['server_group']
                    server.save()
                    server_group.servers.add(server)
                    
                    form = ServerUnitInGroupForm(initial={'group_id':group_id})
                    dictionary = {'form' : form}
                    dictionary.update(csrf(request))
                    render_new_server_form = render_to_response('server_form.html',dictionary, context_instance = RequestContext(request))
                    
                    servers_renders = serverGroupClass().renderGroupServers(request, GroupServer.objects.get(id=group_id))
                    html = ''
                    for render in servers_renders:
                        html = html + render.content
                    
                    message = {"success":True, 
                               'form':render_new_server_form.content,
                               'servers': html
                               }
                else:
                    new_server_form =  {'form' : form}
                    new_server_form.update(csrf(request))
                    render_form = render_to_response('server_form.html', new_server_form, context_instance = RequestContext(request))
                    message = {"success":False,"form": render_form.content}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json') 
        except Exception as e:
            print e
    
    def executeScripts(self, request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group = forms.IntegerField().clean(request.GET['servergroup'])
                server_group = GroupServer.objects.get(id=group)
                scripts = server_group.scripts.all()
                scripts = " && ".join(x.script for x in scripts[::-1])
                hosts = server_group.serverList()
                results = fbt.execute(executeCommand,
                                     command=scripts,
                                     hosts=hosts)
                html = ''
                conv = Ansi2HTMLConverter()
                for result in results:
                    text = "".join(results[result])
                    text = text.decode("UTF-8")
                    html = "<pre>" + result + "</pre> " + conv.convert(text) +'<br>' + html
                message = {"success":True,
                           'html':html}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json')
            return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')       
    
    def deleteServer(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                form =  ServerFromGroupForm(request.POST)
                if form.is_valid():
                    group = GroupServer.objects.get(id=form.cleaned_data['group_id'])
                    group.servers.remove(ServerUnit.objects.get(id=form.cleaned_data['server_id']))
                    group.save()
                    servers_renders = serverGroupClass().renderGroupServers(request, group)
                    html = ''
                    for render in servers_renders:
                        html = html + render.content
                    message = {"success":True, "servers":html}
                else:
                    message = {"success":False}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json')
        except Exception as e:
            print e
            
    def editServer(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                message = {"success":True}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json')
        except Exception as e:
            print e
    def openServerGroup(self, request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group = forms.IntegerField().clean(request.GET['servergroup'])
                server_group = GroupServer.objects.get(id=group)
                group_scripts = GroupServer.objects.get(id=group).scripts.all()
                script_forms = []
                for script in group_scripts[::-1]:
                    form = EditScriptForm(instance=script)
                    script_forms.append(form)    
                dictionary = {'forms' : script_forms, 'server_group':server_group}
                dictionary.update(csrf(request))
                render_script_edit_forms = render_to_response('script_edit_list_form.html', dictionary, context_instance = RequestContext(request))

                script_form = ScriptForm()
                dictionary = {'form' : script_form}
                dictionary.update(csrf(request))
                render_script_form = render_to_response('script_form.html', dictionary, context_instance = RequestContext(request))

                form = ServerUnitInGroupForm(initial={'group_id':group})
                dictionary = {'form' : form}
                dictionary.update(csrf(request))
                render_new_server_form = render_to_response('server_form.html',dictionary, context_instance = RequestContext(request))

                form = EditServerGroupForm(instance=server_group)
                dictionary = {'form' : form}
                dictionary.update(csrf(request))
                render_edit_server_form = render_to_response('server_group_form.html',dictionary, context_instance = RequestContext(request))
                
                form = DeleteServerGroupForm(instance=server_group)
                dictionary = {'form' : form}
                dictionary.update(csrf(request))
                render_group_delete =  render_to_response('server_group_form.html', dictionary, context_instance = RequestContext(request))
                
                form = SearchServerGroupForm(initial={'user':request.user})
                dictionary = {'form' : form}
                dictionary.update(csrf(request))
                render_search_group_server = render_to_response('server_group_search_group.html', dictionary, context_instance = RequestContext(request))
                
                servers_render = serverGroupClass().renderGroupServers(request, server_group)
                dictionary = {'servers_render' : servers_render}
                dictionary.update(csrf(request))
                render_servers =  render_to_response('server_group_servers_list.html', dictionary, context_instance = RequestContext(request))
    
                dictionary = {'render_script_form' : render_script_form.content, 'group_scripts':group_scripts, 'server_group':server_group, 'edit_script_forms':render_script_edit_forms.content}
                render_scripts = render_to_response('server_group_scripts_list.html', dictionary, context_instance = RequestContext(request))

                dictionary = {'server_group':server_group, 
                              'render_servers':render_servers.content, 
                              'render_scripts':render_scripts.content, 
                              'new_server':render_new_server_form.content,
                              'delete_group':render_group_delete.content,
                              'edit_group':render_edit_server_form.content,
                              'search_group':render_search_group_server.content}
                return render_to_response('server_group_view.html', dictionary, context_instance = RequestContext(request))
            
            return HttpResponseRedirect('/user/login') 
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')
    
    def serverGroupServers(self,request):
        try:
            if request.is_ajax() and request.user != AnonymousUser():
                group = forms.IntegerField().clean(request.GET['group'])
                group_servers = GroupServer.objects.get(id=group).servers.all()
                group_scripts = GroupServer.objects.get(id=group).scripts.all()
                dictionary = {'group_servers':group_servers, 'group_scripts':group_scripts}
                render_servers_scripts = render_to_response('server_group_serverscript_list.html', dictionary, context_instance = RequestContext(request))
                message = {"success":True, 'serverlistandscripts':render_servers_scripts.content}
                json_r = simplejson.dumps(message)
                return HttpResponse(json_r, 'application/json')
            return HttpResponseRedirect('/user/login') 
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')
    
    def myServerGroups(self, request):
        try:
            if request.user != AnonymousUser():
                server_groups = GroupServer.objects.filter(user_group__in = UserGroup.objects.filter(owner=request.user)).order_by('-id')
                dictionary = {'server_groups':server_groups}
                render_server_group_list = render_to_response('server_group_list.html', dictionary)
                
                dictionary = {'server_group_list': render_server_group_list.content}
                return render_to_response('server_group_list_view.html', dictionary)
            else:
                return HttpResponseRedirect('/user/login')
        except:
            return HttpResponseRedirect('/user/login')
    
    def newServerGroup(self,request):
        if request.is_ajax():
            post_temp = request.POST.copy()
            post_temp['user'] = request.user
            form = ServerGroupForm(post_temp)
            if form.is_valid():
                server_group = form.save(commit=False)
                server_group.save()
                message = {"success":True }
            else:
                new_server_form =  {'form' : form}
                new_server_form.update(csrf(request))
                render_form = render_to_response('server_group_form.html', new_server_form, context_instance = RequestContext(request))
                message = {"success":False,"form":render_form.content}
            json_r = simplejson.dumps(message)
        else:
            json_r  = 'fail'
        return HttpResponse(json_r, 'application/json')
