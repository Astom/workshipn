#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.forms.forms import Form
from django.forms.models import ModelForm
from django.forms.widgets import TextInput, PasswordInput
from django.utils.translation import ugettext_lazy as _

from usuarios.models import UserGroup


class UserRegisterForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['username', 'first_name', 'last_name', 'email', 'password']
        
    class Meta:
        model = User
        include = ('username', 'first_name', 'last_name', 'email', 'password')
        exclude = ('is_staff','is_active','is_superuser','date_joined',
               'last_login','groups', 'user_permissions')
        widgets = {
            'password' : PasswordInput(attrs={'placeholder': 'Password','class': 'form-control'}),
            'username' : TextInput(attrs={'placeholder': 'User Name','class': 'form-control'}), 
            'first_name' : TextInput(attrs={'placeholder': 'First Name','class': 'form-control'}), 
            'last_name' : TextInput(attrs={'placeholder': 'Last Name','class': 'form-control'}), 
            'email' : TextInput(attrs={'placeholder': 'email@email.com','class': 'form-control'}), 
        }
 
class UserLoginForm(Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'User name','class': 'form-control'}), label='User Name', max_length=254)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password','class': 'form-control'}))

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user_cache = authenticate(username=username, password=password)
            if user_cache is None:
                self._errors["username"] = self.error_class(['Wrong username or password.'])
            elif not user_cache.is_active:
                self._errors["username"] = self.error_class([_("This account is inactive.")])

        return self.cleaned_data
    
class UserGroupForm(ModelForm):
    class Meta:
        model = UserGroup
        include = ('group_name')
        exclude = ('owner', 'creation_date', 'last_update')
        widgets = {
            'group_name' : TextInput(attrs={'placeholder': 'Group Name','class': 'form-control'}),
        }