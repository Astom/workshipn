#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.contrib.auth.models import User
from django.db import models

class UserGroup(models.Model):
    group_name = models.CharField(max_length = 100, verbose_name = 'Group Name', blank=False, null=False)
    owner = models.ForeignKey(User, verbose_name = 'Owner', blank=False, null=False)
    creation_date = models.DateField(blank=True, null=True)
    last_update = models.DateField(blank=True, null=True)
    
    def __unicode__(self):
        return self.group_name

class UserGroupUser(models.Model):
    group = models.ForeignKey(UserGroup, verbose_name = 'Group Name')
    member = models.ForeignKey(User, verbose_name = 'Owner')
    join_date = models.DateField(blank=True, null=True)
