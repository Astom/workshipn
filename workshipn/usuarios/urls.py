#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.conf.urls import patterns, url
from usuarios.views import mainClass

urlpatterns = patterns('usuarios.views',
    url(r'register/confirm/{0,1}', mainClass().confirmRegisterForm),
    url(r'login/confirm_sm/{0,1}', mainClass().confirmLoginForm, kwargs={'template':'user_login_form_mini.html'}),
    url(r'login/confirm/{0,1}', mainClass().confirmLoginForm, kwargs={'template':'user_login_form.html'}),
    url(r'login/{0,1}', mainClass().loginView),
    url(r'logout/{0,1}', mainClass().logOut),
    url(r'register/{0,1}', mainClass().registerView),
    url(r'panel/deleteusergroup/{0,1}', mainClass().deleteUserGroup),
    url(r'panel/members/{0,1}', mainClass().getMembers),
    url(r'panel/{0,1}', mainClass().panelView),
    url(r'newusergroup/{0,1}', mainClass().newUserGroup),
    url(r'myusergroups/{0,1}', mainClass().myUserGroups),
    url(r'', mainClass().loginView),
)