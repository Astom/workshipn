#!/usr/bin/python
# -*- coding: UTF-8 -*-
import datetime

from django import forms
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.models import User, AnonymousUser
from django.core.context_processors import csrf
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import simplejson

from server.form import ServerUnitForm
from server.models import ServerUnit
from server_group.form import ServerGroupForm
from server_group.models import GroupServer
from usuarios.form import UserRegisterForm, UserLoginForm, UserGroupForm
from usuarios.models import UserGroup, UserGroupUser


class mainClass():
    def panelView(self, request):
        try:
            if request.user != AnonymousUser():
                form = UserGroupForm()
                user_group_form = {'form' : form}
                user_group_form.update(csrf(request))
                render_user_group_form = render_to_response('user_group_form.html', user_group_form, context_instance = RequestContext(request))
                user_group_recent = UserGroup.objects.filter(owner=request.user).order_by('-id')[:3]
                
                form = ServerGroupForm(initial={'user':request.user})
                server_group_form = {'form' : form}
                server_group_form.update(csrf(request))
                render_server_group_form = render_to_response('server_group_form.html',server_group_form, context_instance = RequestContext(request))
                server_group_recent = GroupServer.objects.filter(user_group__in = UserGroup.objects.filter(owner=request.user)).order_by('-id')[:3]
                
                form = ServerUnitForm(initial={'user':request.user})
                server_form = {'form' : form}
                server_form.update(csrf(request))
                render_server_form = render_to_response('server_form.html',server_form, context_instance = RequestContext(request))
                server_recent = GroupServer.objects.filter(user_group__in = UserGroup.objects.filter(owner=request.user))
                servers = ServerUnit.objects.none()
                for server in server_recent:
                    servers = servers | server.servers.all()
                servers = servers.order_by('-id')[:3]
                
                vars = { 'user_group_form' : render_user_group_form.content, 
                        'user_group_recent' : user_group_recent,
                        'server_group_recent':server_group_recent,
                        'server_recent':servers,
                        'server_group_form': render_server_group_form.content,
                        'server_form':render_server_form.content}
                
                return render_to_response('user_panel_view.html', vars)
            else:
                return HttpResponseRedirect('/user/login')
        except Exception as e:
            print e
            return HttpResponseRedirect('/user/login')
    
    def loginView(self, request):
        form = UserLoginForm()
        user_login_form =  {'form' : form}
        user_login_form.update(csrf(request))
        render_form = render_to_response('user_login_form.html', user_login_form, context_instance = RequestContext(request))
        vars = { 'form' : render_form.content }
        return render_to_response('user_login_view.html', vars)
    
    def confirmLoginForm(self, request, **kwargs):
        if request.is_ajax():
            form = UserLoginForm(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data.get("username"), password=form.cleaned_data.get("password"))
                login(request, user)
                message = {"success":True }
            else:
                login_user_form =  {'form' : form}
                login_user_form.update(csrf(request))
                render_form = render_to_response(kwargs['template'], login_user_form, context_instance = RequestContext(request))
                message = {"success":False,"form":render_form.content}
            json_r = simplejson.dumps(message)
        else:
            json_r  = 'fail'
        return HttpResponse(json_r, 'application/json')
    
    def logOut(self,request):
        try:
            logout(request)
            return HttpResponseRedirect('/')
        except:
            return HttpResponseRedirect('/')
    
    def registerView(self, request):
        form = UserRegisterForm()
        user_register_form =  {'form' : form}
        user_register_form.update(csrf(request))
        render_form = render_to_response('user_register_form.html', user_register_form, context_instance = RequestContext(request))
        vars = { 'form' : render_form.content }
        return render_to_response('user_register_view.html', vars)
    
    def confirmRegisterForm(self, request):
        if request.is_ajax():
            form = UserRegisterForm(request.POST)
            if form.is_valid():
                #FALTA LOGICA DE PERMISOS DE USUARIO
                user = User.objects.create_user(username=form.cleaned_data['username'])
                user.set_password(form.cleaned_data['password'])
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.email = form.cleaned_data['email']
                user.is_active = True
                user.save()
                message = {"success":True }
            else:
                new_user_form =  {'form' : form}
                new_user_form.update(csrf(request))
                render_form = render_to_response('user_register_form.html', new_user_form, context_instance = RequestContext(request))
                message = {"success":False,"form":render_form.content}
            json_r = simplejson.dumps(message)
        else:
            json_r  = 'fail'
        return HttpResponse(json_r, 'application/json')
    
    def newUserGroup(self, request):
        try:
            if request.is_ajax():
                form = UserGroupForm(request.POST)
                if form.is_valid():
                    new_group = form.save(commit=False)
                    if request.user != AnonymousUser():
                        new_group.owner = request.user
                        new_group.creation_date = datetime.datetime.now()
                        new_group.last_update = datetime.datetime.now()
                        new_group.save()
                        message = { "success":True }
                    else:
                        message = {"success":False }
                else:
                    login_user_form =  {'form' : form}
                    login_user_form.update(csrf(request))
                    render_form = render_to_response('user_group_form.html', login_user_form, context_instance = RequestContext(request))
                    message = {"success":False,"form":render_form.content}
                json_r = simplejson.dumps(message)
            else:
                json_r  = 'fail'
            return HttpResponse(json_r, 'application/json')
        except:
            message = {"success":False }
            json_r = simplejson.dumps(message)
            return HttpResponse(json_r, 'application/json')
        
    def myUserGroups(self, request):
        user_owned_groups = UserGroup.objects.filter(owner=request.user).order_by('-id')
        vars = { 'user_owned_groups' : user_owned_groups}
        vars.update(csrf(request))
        render_owned = render_to_response('user_my_groups_list.html', vars, context_instance = RequestContext(request))
        
        user_works_on = UserGroupUser.objects.filter(member=request.user).order_by('-id')
        vars = { 'user_works_on' : user_works_on}
        vars.update(csrf(request))
        render_works_on = render_to_response('user_working_on_list.html', vars, context_instance = RequestContext(request))
        
        vars = { 'user_owned_groups' : render_owned.content, 'user_works_on':render_works_on.content}

        return render_to_response('user_groups_view.html', vars)
    
    def deleteUserGroup(self, request):
        try:
            if request.is_ajax():
                if request.user != AnonymousUser():
                    group_id = forms.IntegerField().clean(request.POST['group']) 
                    group = UserGroup.objects.get(owner=request.user,id=group_id)
                    if isinstance( group, ( int, long ) ):
                        group.delete()
                        
                        user_owned_groups = UserGroup.objects.filter(owner=request.user).order_by('-id')
                        vars = { 'user_owned_groups' : user_owned_groups}
                        vars.update(csrf(request))
                        render_owned = render_to_response('user_my_groups_list.html', vars, context_instance = RequestContext(request))
                        
                        message = {"success":True, 'owned_groups':render_owned.content }
                        json_r = simplejson.dumps(message)
                        return HttpResponse(json_r, 'application/json')
            else:
                json_r  = 'fail'
            return HttpResponse(json_r, 'application/json')
        except Exception:
            json_r  = 'fail'
            return HttpResponse(json_r, 'application/json')
        
    def getMembers(self, request):
        try:
            if request.is_ajax():
                if request.user != AnonymousUser():
                    group = forms.IntegerField().clean(request.GET['group'])
                    if isinstance( group, ( int, long ) ):
                        group_obj = UserGroup.objects.get(id=group)
                        members =  UserGroupUser.objects.filter(group=group_obj)
                        vars = { 'members' : members, 'owner' : group_obj.owner}
                        render_members = render_to_response('user_group_member_list.html', vars, context_instance = RequestContext(request))

                        message = {"success":True, 'members':render_members.content}
                        json_r = simplejson.dumps(message)
                        return HttpResponse(json_r, 'application/json')
            json_r  = 'fail'
            return HttpResponse(json_r, 'application/json')
        except Exception as e:
            print e
            json_r  = 'fail'
            return HttpResponse(json_r, 'application/json')
        