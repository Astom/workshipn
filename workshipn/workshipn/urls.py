from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'user/{0,1}', include('usuarios.urls')),
    url(r'servergroup/{0,1}', include('server_group.urls')),
    url(r'server/{0,1}', include('server.urls')),
    url(r'script/{0,1}', include('scripts.urls')),
    url(r'', include('main_site.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
